# modulebox_monitoring: Reads and logs temperature from several
# NTC thermistors, attached to the module testing jigs. NTCs
# are connected to a Raspberry Pi on a 1-wire interface.
#
# Cole Helling, January 2022

import pigpio
import helpers
import os
import glob
import time
import argparse
import json
import logging

from datetime import datetime
from datetime import date

parser = argparse.ArgumentParser(
    description="Read temperature from multiple NTCs and temperature and humidity from an SHT21 sensor."
)

parser.add_argument(
    "-c", dest="configfile", type=str, default="", help="Path to configuration json."
)

parser.add_argument(
    "-t",
    dest="runtime",
    type=int,
    default=20,
    help="Total measurement runtime in minutes.",
)


args = parser.parse_args()


if __name__ == "__main__":

  # path to configuration file
  configfile = args.configfile

  try:
    conf = json.load(open(configfile))
  except IOError as errio:
    logging.getLogger().error('Cannot load configfile, using default values is not recommended.')
    conf = dict()

  # Sensors
  connected_sensors = conf.get('Sensors', dict()).get('connected_sensors', 'autodiscover')
  # Configuration
  _LOG_LEVEL = conf.get('Configuration', dict()).get('log_level', 'ERROR')
  _LOG_NAME = conf.get('Configuration', dict()).get('log_file', 'modulebox_monitoring.log')
  _LOG_DIR = conf.get('Configuration', dict()).get('log_directory', '/var/log/modulebox_monitoring')
  # Devices
  oneWire_dir    = conf.get('oneWire_dir', '/sys/bus/w1/devices/')
  Device_aliases = conf.get('Device_aliases', 'autodiscover')
   # Grafana
  grafana_active = conf.get('Grafana', dict()).get('active', True)
  grafana_protocol = conf.get('Grafana', dict()).get('protocol', 'https')
  grafana_server = conf.get('Grafana', dict()).get('server', 'example.com/influx/')
  grafana_port   = conf.get('Grafana', dict()).get('port')
  grafana_user   = conf.get('Grafana', dict()).get('user', 'cleanroom')
  grafana_password= conf.get('Grafana', dict()).get('password', 'borkborkbork')
  database       = conf.get('Grafana', dict()).get('database', 'telegraf')
 
  logging.basicConfig(filename=f'{_LOG_DIR}/{_LOG_NAME}', level=_LOG_LEVEL)
  # -------------------------------------------------------------- #
  #                           Sensors                              #
  # -------------------------------------------------------------- #
  try:
    pi = pigpio.pi()
    with helpers.TemperatureHumidityMonitor(pi, args) as sht21:
      # if sensors not specified, autodiscover them. Can handle any string
      if (connected_sensors == "autodiscover" or type(connected_sensors) == str):
        connected_sensors = sht21.sensor_autodiscover()
        helpers.edit_Sensors(configfile, connected_sensors)
      
      # store values for logging
      SHT_sensors      = []
      SHT_temperatures = []
      SHT_humidities   = []
      # scan over listed sensors
      for sensor in connected_sensors:

        # currently used to stop spurious results coming from unattached
        # sensors.
        sht21.scan_mux()
        # set proper multiplexer and set channel based on sensor     
        sht21.set_mux(sensor)

        # get temperature and humidity readings from sensor 
        logging.getLogger().debug(f'sensor_str: "SHT%s" {sensor}')
        sensor_str = 'SHT%s' %sensor 
        temperature = sht21.read_temperature()
        logging.getLogger().debug(f'temperature: {temperature}')
        humidity    = sht21.read_humidity()
        logging.getLogger().debug(f'humidity: {humidity}')

        # -999 for either values means an error occured, do not write values
        if ((temperature != -999) and (humidity != -999)):
          
          # store values for logging
          SHT_sensors.append(sensor_str)
          SHT_temperatures.append(temperature)
          SHT_humidities.append(humidity)

          # send data to grafana server
          if (grafana_active):
            helpers.SHT_send2influxdb(grafana_protocol, grafana_server, grafana_port, grafana_user, grafana_password, sensor_str, temperature, humidity, database)

  except Exception as e:
    logging.getLogger().error(f'{e}\n\tError creating connection to i2c.')
 
#  # -------------------------------------------------------------- #
#  #                           Devices                              #
#  # -------------------------------------------------------------- #
#  
#  # discover devices if none given
#  if (Device_aliases == 'autodiscover'):
#    Device_aliases = helpers.device_autodiscover(oneWire_dir)
#    # if configfile given, edit it with devices found
#    if (bool(conf)):
#      helpers.edit_Devices(configfile, Device_aliases)
#
#  # to store sensor aliases and temperatures for database upload
#  NTC_aliases        = []
#  NTC_temperatures   = []
#
#  # loop over devices and grab temperature readings
#  for name, alias in Device_aliases.items():
#    
#    # store temperature and alias for grafana
#    NTC_temperatures.append(helpers.read_temp(oneWire_dir, name))
#    NTC_aliases.append(alias)
#
#  if (grafana_active and bool(Device_aliases)):
#    helpers.NTC_send2influxdb(grafana_protocol, grafana_server, grafana_port, grafana_user, grafana_password, NTC_aliases, NTC_temperatures, database)
# 
#  # -------------------------------------------------------------- #
#  #                           Logging                              #
#  # -------------------------------------------------------------- #
#  if (logoutput):
#    # create file and make header for output file only on first pass
#      logging.getLogger().info(f'{NTC_aliases}, {SHT_sensors}')
#      logging.getLogger().info(f'{NTC_aliases}, {SHT_sensors}, {NTC_temperatures}, {SHT_temperatures}, {SHT_humidities} ')
#
#
