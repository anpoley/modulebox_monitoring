
#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@author: Andre Poley
@contact: andre.poley@sfu.ca
@copyright: (c) 2022 by Andre Poley, Vancouver
@license: MIT
@version: 0.0.0
@summary: Write data to influxdb
'''
import socket
import sys
import logging
import requests


def prepare_data(log):
    ''' Prepare the logged data for a batched influx line interface query
    @param log: A group of comma seperated values
    @type log: [[int][int][float][float]]
    '''
    data = []
    for i in range(len(log)):
        data.append(
            f'{_INFLUX_DATABASE},host={socket.gethostname()}-{log[i][1]} temperature={log[i][2]},humidity={log[i][3]} {log[i][0]}')
    return data


def post_to_database(args):
    ''' Post the batched data to the database
    @param args: prepared data
    @type args: str
    '''
    params = (
        ('precision', 's'),
        ('db', f'{_INFLUX_DATABASE}'),
    )
    data = ''
    for i in range(len(args)):
        data = f'{data}\n{args[i]}'
    try:
        logging.getLogger().debug(f'Influx query: {data}')
        # response = requests.post(
        requests.post(
            f'http://{_INFLUX_SERVER}/write',
            params=params,
            data=data,
            verify=False,
            auth=(f'{_INFLUX_USER}', f'{_INFLUX_PASSWORD}'))
    except requests.exceptions.HTTPError as errh:
        logging.getLogger().error(f'Http Error: {errh}')
    except requests.exceptions.ConnectionError as errc:
        logging.getLogger().errorp(f'Error Connecting: {errc}')
    except requests.exceptions.Timeout as errt:
        logging.getLogger().error(f'Connection Timout: {errt}')
    except requests.exceptions.TooManyRedirects as errr:
        logging.getLogger().error(f'Too many redirects: {errr}')
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        logging.getLogger().error(f'Request Exception: {e}')
        sys.exit(1)
    finally:
        logging.getLogger().debug('Wrote to influx')


def debug():
    logging.getLogger().debug('_INFLUX_SERVER\t\t{_INFLUX_SERVER}')
    logging.getLogger().debug('_INFLUX_DATABASE\t\t{_INFLUX_DATABASE}')
    logging.getLogger().debug('_INFLUX_USER\t\t\t{_INFLUX_USER}')
    logging.getLogger().debug('_INXLUX_PASSWORD\t\t{_INFLUX_PASSWORD}')
