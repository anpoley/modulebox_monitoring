import os
import time
import glob
import json
import logging
import requests
import argparse
import pigpio
from datetime import datetime
from datetime import date
from string import ascii_uppercase


def start_output_file(date_string, NTC_aliases, SHT_sensors, start_time):
    """
    start_file( () date_string (string array) NTC_aliases, (string array) SHT_sensors,
                (int) start_time )

    -- NTC_aliases    : convenient names for the NTCs
       (string array)

    -- SHT_sensors    : name of SHT sensors
       (string array)

    -- start_time     : UTC time
       (int)
    """

    # create a unique file using today's date and start time.
    outputfilename = date_string + ".txt"
    with open(outputfilename, "w+") as f:
        save_string = "time \t\t"

        for alias in NTC_aliases:
            save_string += alias + " \t"

        for sensor in SHT_sensors:
            save_string += sensor + "-T" "\t"
            save_string += sensor + "-RH" "\t"

        f.write(save_string)
        f.write("\n")
        f.close()


def add_values_to_output(
    current_time,
    date_string,
    NTC_aliases,
    SHT_sensors,
    NTC_temperatures,
    SHT_temperatures,
    SHT_humidities,
):
    """
    add_values_to_input( (int) current_time, (string_array) NTC_aliases, (string_array) SHT_sensors,
                         (float array) NTC_temperatures, (float array) SHT_temperatures,
                         (float array) SHT_humidities )

    -- NTC_aliases      : convenient names for the NTCs
       (string array)

    -- SHT_sensors      : name of SHT sensors
       (string array)

    -- NTC_temperatures : temperatures for the NTCs
       (float array)

    -- SHT_temperatures : array of floats with SHT temperatures
       (float array)

    -- SHT_humidities   : array of floats with SHT humidities
       (float array)

    """

    outputfilename = date_string + ".txt"

    with open(outputfilename, "a+") as f:
        save_string = str(current_time) + "\t"

        for temp in NTC_temperatures:
            save_string += "{:.2f}".format(temp) + "\t"

        for temp, humi in zip(SHT_temperatures, SHT_humidities):
            save_string += "{:.2f}".format(temp) + "\t"
            save_string += "{:.2f}".format(humi) + "\t"

        f.write(save_string)
        f.write("\n")
        f.close()


def device_autodiscover(oneWire_dir):
    """
    device_autodiscover( (string) oneWire_dir )

    -- oneWire_dir : string containing the path to the one-wire devices
       (string)

    This function looks in the directory containing all known one-wire devices
    with names starting with a "28", which is the start for all DS18B20 NTCs.
    Each of the devices and their aliases, of the form NTC-X (X = A,B,...,Z), is
    saved in a dictionary to be used to upload to the database.
    """

    Device_aliases = dict()
    device_dirs = glob.glob(oneWire_dir + "28*")

    for device_dir, letter in zip(device_dirs, ascii_uppercase):
        name = device_dir.split("/")[-1]
        Device_aliases[name] = "NTC-" + letter

    return Device_aliases


def edit_Sensors(configfile, connected_sensors):
    """
    edit_Sensors( (string) configfile, (list) connected_sensors)

    If the configuration was set to autodiscover, or no sensors
    were listed, this function will edit the config file, adding the
    connected_sensors field or editing it with the sensors which have
    been found
    """

    with open(configfile, "r") as f:
        conf = json.load(f)

    conf["Sensors"]["connected_sensors"] = connected_sensors

    with open(configfile, "w") as f:
        json.dump(conf, f, indent=4)


def edit_Devices(configfile, Device_aliases):
    """
    edit_Devices( (string) configfile, (dict) Device_aliases )

    -- configfile     : path to the configuration json
       (string)

    -- Device_aliases : dictionary of devices found in the oneWire directory
       (dict)

    This function opens the configfile and sets the devices found in the
    autodiscover function in the Device_aliases field.
    """

    # open configuration file, load dictionary
    with open(configfile, "r") as f:
        conf = json.load(f)

    # set the 'Device_aliases' field as empty dict
    conf["Device_aliases"] = dict()

    # set the 'Device_aliases' field to the dictionary created in the
    # autodiscover function
    conf["Device_aliases"] = Device_aliases

    # write json to file
    with open(configfile, "w") as f:
        json.dump(conf, f, indent=2)


def read_temp(oneWire_dir, name):
    """
    read_temp( (string) oneWire_dir, (string) name )

    -- oneWire_dir : path to the directory with all one-wire devices detected
       (string)

    -- name        : name of the device
       (string)

    This function opens the device file 'w1_slave', which contains information
    in the form:

    55 01 55 05 7f a5 81 66 c3 : crc=c3 YES
    55 01 55 05 7f a5 81 66 c3 t=21312

    If a 'YES' is not present in the first line, keep reading it until it is.
    The temperature is the value in the second line, divided by 1000.

    """
    temp = -999
    # file containing temperature reading from device
    device_file = oneWire_dir + name + "/w1_slave"
    # open device file
    with open(device_file, "r") as f:
        lines = f.readlines()
    # keep checking until valid
    while "YES" not in lines[0]:
        time.sleep(0.2)
        lines = read_temp_raw()
    temp = float(lines[1].split("=")[1].strip("\n")) / 1000.0
    return temp


def SHT_send2influxdb(protocol, server, port, user, password, sensor, temperature, humidity, database):
  '''
  send2influxdb( (string server, (string) port, (string) sensor_str,
                 (float) temperture, (float) humidity, (string) database)
  
  This function takes in the sensor values and sends them to out database. The
  function was taken from Carlos Garcia Argos' script and modified by Zhengcheng
  Tao, then modified by Cole Helling
  
  -- server (string)
      IP address to the virtual machine holding the data
  
  -- port (string)
      port number the VM is expecting for data
  
  -- sensor (string)
      name of the sensor
  
  -- temperature (float)
      value extracted from the I2C readout of the SHT sensor
  
  -- humidity (float)
      value extracted from the I2C readout of the SHT sensor
  
  -- database (string)
      name of the database

  -- user (string)
     name of the database user

  -- password (string)
     password for the database user
  '''
  # data
  hostname = os.uname()[1]
  timestamp = int(time.time())
  measurement_name = 'SHT'
  #measurement_name = 'bork-'+sensor

  # via protocol
  destination = f'{protocol}://{server}'
  # destination argument
  argument = "write?precision=s"
  if (database):
    argument += f'&db={database}'
  if port:
    destination += f':{port}{argument}'
  else:
    destination += f'{argument}'

  logging.getLogger().debug(f'destination:\n\n{destination}')
#  data = "{},host={},sensor={} ".format(measurement_name, hostname, 'NTCs')
  data = f'{measurement_name},host={hostname},sensor={sensor} temperature={temperature},humidity={humidity} {timestamp}'
  logging.getLogger().info(f'{data}')

  # send data to destination
  try:
    requests.post(destination, data = data, timeout = 2.50, verify=True, auth=(f'{user}', f'{password}'))
  except:
    logging.getLogger().error('failed to write SHT data to database')
    pass

def NTC_send2influxdb(http, server, port, user, password, sensors, temperatures, database=""):
  '''
  NTC_send2influxdb( (string) server, (string) port, (string array) sensors,
                     (float array) temperatures, (string) database)

    -- server (string)
        IP address to the virtual machine holding the data

    -- port (string)
        port number the VM is expecting for data

    -- sensor (string)
        name of the sensor

    -- temperature (float)
        value extracted from the I2C readout of the SHT sensor

  -- temperatures  : temperatures (in degrees C) from each of the NTCs
     (float array) 
  
  -- database      : name of the database
     (string)

  -- user (string)
     name of the database user

  -- password (string)
     password for the database user
  '''
  # destination
  destination = f'{http}://{server}'
  argument = "write?precision=s"
  if (database):
    argument += f'&db={database}'
  if port:
    destination += f':{port}{argument}'
  else:
    destination += f'{argument}'
  # data
  hostname = os.uname()[1]
  timestamp = int(time.time())
  #measurement_name = hostname + '_' + 'NTCs'
  measurement_name = 'NTC'
  # start data string
  data = f'{measurement_name},host={hostname},sensor=NTC '
  # add temperatures from each sensor 
  for sensor,temp in zip(sensors, temperatures):
    data += f'{sensor}_temperature={temp}, '
  data += f'{timestamp}'
  # send data to destination
  try:
    logging.getLogger().debug(f'{destination}{data}')
    requests.post(destination, data = data, timeout = 2.50, verify=True, auth=(f'{user}', f'{password}'))
  except:
    logging.getLogger().error('Failed to write NTC data to database')
    pass

class TemperatureHumidityMonitor:
    """
    Reads out the temperature and humidity from one or more
    SHT21 sensors.

    __init__
    Variables are assigned via a json file or given a default value
    if not specified.

    set_mux
    Selects the appropriate multiplexer and sets the communication channel
    for the sensor to be read out

    read_temperature
    Reads bytes from sensor and converts the values to Celsius per the
    sensor's datasheet

    read_humidity
    Reads bytes from sensor and converts the values to percent per the
    sensor's datasheet
    """

    def __init__(self, pi, args):
        """
        Initialize variables with values from the configuration file,
        or use defaults if either the configuration file was not specified,
        or the fields are not presesent in the file.
        """
        self.configfile = args.configfile

        try:
            conf = json.load(open(self.configfile))
        except:
            logging.getLogger().warning(f'Cannot load configfile, using default values')
            conf = dict()

        # Addresses for the multiplexers
        self.sensors_mux1_addr = conf.get("Sensors", dict()).get("mux1_addr", 112)
        self.sensors_mux2_addr = conf.get("Sensors", dict()).get("mux2_addr", 113)

        # Control Constants
        self._SOFTRESET = conf.get("Sensors", dict()).get("soft_reset", 254)
        self._I2C_ADDRESS = conf.get("Sensors", dict()).get("SHT_address", 64)
        self._TRIGGER_TEMPERATURE_NO_HOLD = conf.get("Sensors", dict()).get(
            "trigger_temp", 243
        )
        self._TRIGGER_HUMIDITY_NO_HOLD = conf.get("Sensors", dict()).get(
            "trigger_humi", 245
        )

        self.pi = pi

        # Reset the multiplexers
        self.mux1 = self.pi.i2c_open(1, self.sensors_mux1_addr)
        self.pi.i2c_write_byte(self.mux1, self._SOFTRESET)

        self.mux2 = self.pi.i2c_open(1, self.sensors_mux2_addr)
        self.pi.i2c_write_byte(self.mux2, self._SOFTRESET)

        # Reset the SHT sensors
        self.sensor = self.pi.i2c_open(1, self._I2C_ADDRESS)
        self.pi.i2c_write_byte(self.sensor, self._SOFTRESET)

        # Sleep briefly
        time.sleep(0.015)

    def read_temperature(self):
        """
        Reads in the temperature from the sensor.
        Note that this call waits 250ms to allow the sensor
        to return the data

        The function takes the first two bytes of data and converts
        the temperature in celcius by using the following function:

        T = 46.85 + 172.72 * (sensor_temp / 2^16)

        Per the sensor documentation, the last two bits of the second
        byte are stat bits, and should be set to zero prior to calculating
        the temperature.
        """

        try:
            # trigger temperature reading
            self.pi.i2c_write_byte(self.sensor, self._TRIGGER_TEMPERATURE_NO_HOLD)
            time.sleep(0.250)

            # read two bytes for data, one for the checksum
            cnt, data = self.pi.i2c_read_device(self.sensor, 3)

            if len(data) == 0:
                raise RuntimeError("Error reading temperature")

            # The last two bits (stat bits) of the second byte must be set to zero
            # before calculation. For temperature readings, they are both always
            # 0, so no need to change.
            temp = -46.85 + 175.72 * ((data[0] * 2.0**8) + data[1]) / 2.0**16
        except:
            temp = -999

        return temp

    def read_humidity(self):
        """
        Reads the humidity from the sensor.
        Note that this call waits 250ms to allow the sensor
        to return the data

        The humidity function takes the first two bytes of data and
        converts the humidity in percent by using the following function:

        RH = -6.0 + 125.0*(sensor_hum / 2^16)
        """
        try:
            # trigger humidity reading
            self.pi.i2c_write_byte(self.sensor, self._TRIGGER_HUMIDITY_NO_HOLD)
            time.sleep(0.250)

            # read two bytes for data, one for the checksum
            cnt, data = self.pi.i2c_read_device(self.sensor, 3)

            if len(data) == 0:
                raise RuntimeError("Error reading humidity")

            # The last two bits (stat bits) of the second byte must be set to zero
            # before calculation. For humidity readings, they are always
            # 10, so we need to subract 2 (i.e. 00000010 = 2).
            humidity = -6.0 + 125.0 * ((data[0] * 2.0**8) + (data[1] - 2)) / 2.0**16

        except:
            humidity = -999

        return humidity

    def scan_mux(self):
        """
        It seems that if you read from a multiplexer when
        no sensors are present, you get values when you shouldn't.
        By scanning over each channel on both multiplexers,
        this problem doesn't happen.
        """
        for i in range(0, 8):
            self.pi.i2c_write_byte(self.mux1, 2**i)
            self.pi.i2c_write_byte(self.mux2, 2**i)

    def set_mux(self, idx):
        """
        set_mux(self, (int) sensor_idx

        This function takes in the sensor index 0, 1, ..., 15. Each
        multiplexer has 8 channels, with addresses in powers of two,
        i.e. 2^0, 2^1, ..., 2^15. For sensors on multiplexer 2 (mux2),
        the index is shifted to give the correct channel address.
        """
        if idx < 8:
            self.pi.i2c_write_byte(self.mux1, 2**idx)
        else:
            self.pi.i2c_write_byte(self.mux2, 2 ** (idx - 8))

    def sensor_autodiscover(self):
        """
        If which sensors connected is not known, this
        function will scan over all sensors and return
        the sensor numbers that have valid values.
        """
        # to store connected sensors
        connected_sensors = []
        # scan over sensors
        self.scan_mux()

        # scan channels and look for valid values
        for i in range(0, 15):
            self.set_mux(i)
            # get temperature to test
            t = self.read_temperature()

            if t != -999:
                connected_sensors.append(i)

        if len(connected_sensors) == 0:
            logging.getLogger().error(f'No sensors were discovered, abort ')
            sys.exit(-1)
        else:
            return connected_sensors

    def close(self):
        """
        Closes the I2C connection
        """
        self.pi.i2c_close(self.mux1)
        self.pi.i2c_close(self.mux2)
        self.pi.i2c_close(self.sensor)
        self.pi.stop()

    def __enter__(self):
        """
        Used to enable python with statement support
        """
        return self

    def __exit__(self, type, value, traceback):
        """
        With support
        """
        self.close()
