# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='modulebox_monitoring',
    version='1.1.0',
    description='Log continous sensor data with a raspberry pi.',
    long_description=readme,
    author='Cole Michael Helling, Andre Poley',
    author_email='andre.poley@mailbox.org',
    url='https://gitlab.cern.ch/anpoley/modulebox_monitoring',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
