# ModuleBox_Monitoring

This project uses a Raspberry Pi 3 (model B), several NTC thermistors, and two
SHT21 temperature and humidity sensors. The NTCs measure the temperature of the
module jigs and the SHT21 sensors measure the temperature and humidity of the
air.  Throughout the README, and in the code, NTCs are referred to as `Devices`
and the SHT21s as `Sensors`, just to make sure the distinction is clear. 

More functionality to be added as the project develops.

## Dependencies

This project uses standard python packages which should already be installed:

  * os
  * glob
  * time
  * json
  * argparse
  * requests
  * ascii_uppercase

## DS18B20 NTC Thermistor

For this project, I use the Aideepen DS18B20 Waterproof Digital Temperature
Sensors. These NTCs were purchased on
[Amazon](https://www.amazon.ca/gp/product/B01LY53CED/ref=ppx_yo_dt_b_asin_title_o03_s01?ie=UTF8&psc=1)
and are widely available by other distributors at a fairly low cost.

![](docs/Images/DS18B20.png)

### Enable 1-Wire Communication

These devices use [1-wire](https://pinout.xyz/pinout/1_wire) (w1) communication
to talk to a Raspberry Pi. To enable 1-wire communication, enter the following
into the command line:

``` bash
sudo raspi-config
``` 

Go to `3`, `Interface Options` and then to `P7`, `1-Wire` and enable it. The Pi
will need to be rebooted if 1-Wire communication wasn't previously enabled. This
can also be accomplished by editing the config file in the boot directory:

``` bash
sudo vim /boot/config.txt
```

and adding the following to the bottom of the file:

```
dtoverlay=w1-gpio,gpiopin=4
```

It is not necessary to add the `,gpiopin=4` bit, as gpio-pin 4 (pin 7 on the
Pi) is the default, but just in case you want to change the pin, this is how it
is done. While I don't think it'll be necessary, you can load the w1 drivers by
entering the following into the command line:

``` bash
sudo modprobe w1-gpio
sudo modprobe w1-therm
```

I think these are loaded after 1-wire is enabled, but if I'm incorrect, please
let me know.

### NTC Wiring

The devices have three wires: red (3.3V on pin-1), black (GND on pin-6), yellow
(DATA on pin-7). If setting this up on a breadboard, multiple devices can be
wired in the following manner, as described
[here](https://raspberryautomation.com/connect-multiple-ds18b20-temperature-sensors-to-a-raspberry-pi/),
using a 4.7 kOhm resistor with the data line:

![](docs/Images/DS18B20_breadboard.png)

Be sure to check the wiring carefully.

### Reading the Devices

Once wired up, the devices should automatically show up. They are all found in
the 1-wire directory, located in `/sys/bus/w1/devices`. These sensors all have
names that start with `28-`.

``` bash
ls /sys/bus/w1/devices/

28-3c01d607037c  28-3c01d60718a8  28-3c01d6075b80  28-3c01d6076920  28-3c01d607aa12  w1_bus_master1
```

I've found that when there is a poor connection, you may see several devices
starting with `00-`. If you see those and not the expected devices, check your
connections. As far as I am aware, these device names are stable and unique. To
read the temperature from one of these, let's say device `28-3c01d607037c`,
type the following into the command line:

``` bash
cat /sys/bus/w1/devices/28-3c01d607037c/w1_slave
```

If all is working, you should see the following output:

``` bash
21 01 55 05 7f a5 81 66 63 : crc=63 YES
21 01 55 05 7f a5 81 66 63 t=18062
```

The temperature, in degrees Celcius, is 18062/1000 = 18.062. If you have
several devices connected, you'll need to figure out which physical device
corresponds to the device name. My suggestion is just to hold one of them in
your hand for about 30 seconds and slap a label on it once you've seen which
one it is. This might be easier to do once you have the code up and running,
which is discussed in the [Running the
Program](https://gitlab.cern.ch/chelling/modulebox_monitoring#running-the-program)
section. 

## Custom PiHat Board

I soldered these devices to a custom PiHat breadboard for a more reliable
connection. Below is a schematic for how it was put together on a blank
breadboard purchased on
[Amazon](https://www.amazon.ca/gp/product/B07BF8Z3HS/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1).
There are several varieties of PiHat breadboards, and any should work just
fine. If connecting the SHT PiHat board as well, make sure you have the
appropriate 2x20 GPIO headers.

![](docs/Images/PiHat_Breadboard.png)

## SHT PiHat Board

The PiHat board was designed by Carlos Garcia Argos (whom I thank greatly). It
is designed to connect to up to 16 SHT sensors via 4 16-pin ribbon cables. The
code is designed to work with the
[SHT21](http://www.farnell.com/datasheets/1780639.pdf) temperature and humidity
sensors, and may require a slight reworking if applied to other, similar
sensors. I have another
[project](https://gitlab.cern.ch/chelling/temperaturehumiditymonitor) that is
meant to deal with this board alone, which has details for setting up I2C
communication and troubleshooting most of the issues I encountered.

![](docs/Images/SHT21_PiHat_Board.png)

Starting from the bottom connection in the picture above are the locations for
SHT0-SHT3. The sensor numbering starts at zero with the four right-most pins:
SDA, SLC, VIN, and GND. The next set of four pins to the left is for SHT1 and
the pattern continues in this manner across the 16-pin connector and up as you
go up to SHT15.

## Configuration

A json is used to store the configuration settings to communicate with the
sensors and read the devices to send data to the database.

``` json
{
  "oneWire_dir"   : "/sys/bus/w1/devices/",
  "Device_aliases":
  {
    "28-3c01d607037c": "NTC-A",
    "28-3c01d60718a8": "NTC-B",
    "28-3c01d6075b80": "NTC-C",
    "28-3c01d6076920": "NTC-D",
    "28-3c01d607aa12": "NTC-E"
  },
    "Sensors":
  {
    "connected_sensors" : [0, 1],
    "SHT_address"       : 64,
    "mux1_addr"         : 112,
    "mux2_addr"         : 113,
    "soft_reset"        : 254,
    "trigger_temp"      : 243,
    "trigger_humi"      : 245
  },
  "Grafana":
  {
    "active"   : true,
    "server"   : "206.12.94.115",
    "port"     : "8186",
    "database" : "telegraf"
  }
}
```

### Device Configuration

To make it easier to read, I alias each of the device names with a simpler
naming scheme. Each of the NTCs gets an uppercase letter, NTC-X (X =
A,B,...,Z). If you're planning on having more than 26 devices, you may want to
switch to numbering them. The user can manually enter the devices and their
alias, or have them automatically found. If the `Device_aliases` field has the
entry `autodiscover`, or the entire field is left out of the configuration
file, the script will automatically search the 1-wire directory, alias them in
the order they are found, and replace or create the entry in the configuration
file.

### Sensor Configuration

Originally, the configuration for the SHT21 sensors was designed around using
hexidecimal rather than decimal numbers for the addresses in [Carlos'
code](https://gitlab.cern.ch/cgarciaa/storage-humidity-monitor/-/blob/master/software/storagemonitor.conf),
but I have converted it all to decimal to make it all easier to read for
myself.

The default address for the SHT21 sensors (SHT_address) is 0x40, or 64. The two
8-channel multiplexers (mux1_addr and mux2_addr) have been given the addresses
0x70 and 0x71, or 112 and 113 respectively. The soft-reset command, 0xFE or
254, is used to reset the multiplexers and the sensor. To trigger a temperature
or humidity reading, 0xF3 (243) and 0xF5 (245) are used, respectively.

The user can list the connected sensors, [0,...,15]. If nothing is given, or
``autodiscover`` (Any string will actually trigger it) is given in the
`connected_sensors` field instead, the program will automatically scan all
channels and edit the configuration for future scans.

### Grafana Configuration

We send data to a virtual machine using [InfluxDB](https://www.influxdata.com/)
via the [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/)
server agent. To send data, set the `active` field to `true` and configure the
`server` and `port` fields appropriately.

## Running the Program

The modulebox_monitoring.service systemdunit will run the python script every 60
seconds via modulebox_monitoring.timer
The data will be logged to disk untill the timer is stopped. It can additionally
be sent to an influxdb for external storage.

The measurement interval will not be exactly that specified, but approximately 5
seconds longer (the time it takes to run through the sensors with various
delays to allow for proper data collection). To run the program consolidate the
service unit:

``` bash
sudo systemctl status modulebox_monitoring
tail -f /var/log/modulebox_monitoring/modulebox_monitoring.log
```

Dont use the default values or it will crash with debug on.

## Data Logging

When troubleshooting, or just testing out new functionality, it might be useful to 
output the measurements to a log file. To do this, simply add `-o` flag:

``` bash
/etc/modulebox_monitoring/venv/bin/python3 /etc/modulebox_monitoring/modulebox_monitoring/modulebox_monitoring.py -c /etc/modulebox_monitoring/configuration.json
```

TODO: needs documentation
BREAKING: NTC does not work
Which will output the following: 

``` bash
time 		NTC-A 	NTC-B 	NTC-C 	SHT0-T	SHT0-RH	SHT1-T	SHT1-RH	
1645117442	24.62	24.44	23.88	23.07	33.58	23.58	31.14	
1645117453	24.56	24.44	23.81	23.08	33.57	23.60	31.13	
1645117474	24.50	24.38	23.81	23.09	33.58	23.59	31.14	
1645117495	24.44	24.38	23.81	23.08	33.58	23.59	31.16	
```
